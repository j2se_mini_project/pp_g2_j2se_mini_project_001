package com.company;

public class WrongNumberInputException extends Exception {
    public WrongNumberInputException(String message) {
        super(message);
    }
}
