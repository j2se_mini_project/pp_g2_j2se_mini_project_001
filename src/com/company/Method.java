package com.company;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Method {
    private int displayRow = 5;
    private int currentPage = 1;
    private final ArrayList<Product> products = new ArrayList<>();

    public ArrayList<Product> getProducts() {
        return products;
    }

    Scanner scanner = new Scanner(System.in);
    ConnectionDB connectionDB = new ConnectionDB();
    Query query = new Query();
    Connection connection;

    public void PrintConsole() {
        System.out.println("\t\t\t\t\t\t\t\t\t\tWelcome to");
        System.out.println("\t\t\t\t\t\t\t\t\t Stock Management");
        System.out.print("\n" +
                "  _____    _                                   _____                   _        _____   ___  \n" +
                " |  __ \\  | |                                 |  __ \\                 | |      / ____| |__ \\ \n" +
                " | |__) | | |__    _ __     ___    _ __ ___   | |__) |   ___   _ __   | |__   | |  __     ) |\n" +
                " |  ___/  | '_ \\  | '_ \\   / _ \\  | '_ ` _ \\  |  ___/   / _ \\ | '_ \\  | '_ \\  | | |_ |   / / \n" +
                " | |      | | | | | | | | | (_) | | | | | | | | |      |  __/ | | | | | | | | | |__| |  / /_ \n" +
                " |_|      |_| |_| |_| |_|  \\___/  |_| |_| |_| |_|       \\___| |_| |_| |_| |_|  \\_____| |____|\n" +
                "                                                                                             \n");
        System.out.println("Please Wait loading...");
    }

    public Method() {
    }

    //Todo : use to format date to string ("04-20-21 18:20:50")
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    DecimalFormat df = new DecimalFormat("$#,###,###.00");

    public void warningTable(String msg) {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.center, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table menu = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND, false, "");
        menu.addCell(" " + msg + " ");
        System.out.println(menu.render());
    }

    public void menu() {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table menu = new Table(1, BorderStyle.UNICODE_BOX, ShownBorders.SURROUND, false, "");
        menu.addCell(" *: Display  | R: Read    | W: Write    | U: Update | D: Delete | F: First ", cs);
        menu.addCell(" P: Previous | N: Next    | L: Last     | S: Search | G: Goto   | Se: Set Row ", cs);
        menu.addCell(" Sa: Save    | Ba: Backup | Re: Restore | H: Help   | E: Exit ", cs);
        System.out.println(menu.render());
    }

    public void displayProductsInRow(int displayRow, int currentPage, ArrayList<Product> products) {
        int totalProducts = products.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            int totalPage = totalProducts / displayRow;
            if (totalProducts % displayRow != 0) totalPage += 1;
            if (totalPage < currentPage) currentPage = totalPage;
            int pageIndex = currentPage - 1;
            int startIndex = displayRow * pageIndex;
            int restProducts = (totalProducts - startIndex);
            if (restProducts < displayRow) displayRow = restProducts;
            CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
            Table table = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL, false, "");
            table.addCell("ID", cellStyle);
            table.addCell("Name", cellStyle);
            table.addCell("Unit Price", cellStyle);
            table.addCell("Quantity", cellStyle);
            table.addCell("Imported Date", cellStyle);
            table.setColumnWidth(0, 10, 15);
            table.setColumnWidth(1, 15, 30);
            table.setColumnWidth(2, 15, 25);
            table.setColumnWidth(3, 15, 25);
            table.setColumnWidth(4, 20, 30);

            for (int i = startIndex; i < displayRow + startIndex; i++) {
                Product p = products.get(i);
                table.addCell(p.getId() + "", cellStyle);
                table.addCell(p.getName(), cellStyle);
                table.addCell(df.format(p.getUnitPrice()), cellStyle);
                table.addCell(p.getQty() + "", cellStyle);
                table.addCell(p.getImportedDate(), cellStyle);
            }

            System.out.println(table.render());

            CellStyle cs1 = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
            CellStyle cs2 = new CellStyle(CellStyle.HorizontalAlign.right, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
            Table footer = new Table(2, BorderStyle.DESIGN_CURTAIN_WIDE, ShownBorders.SURROUND, false, "");
            footer.addCell("Page " + currentPage + " of " + totalPage, cs1);
            footer.addCell("Total Record : " + totalProducts, cs2);
            footer.setColumnWidth(0, 37, 60);
            footer.setColumnWidth(1, 40, 60);
            System.out.println(footer.render());
        }
    }

    public void displayProduct(ArrayList<Product> prod) {
        displayProductsInRow(displayRow, currentPage, prod);
    }

    public void insertProductByShortcut(ArrayList<Product> prod, String pName, double price, int qty) throws IOException, SQLException, ClassNotFoundException {
        connection = connectionDB.getConnectionDB();
        int id = query.selectMaxID(connection, "tblproductstemp") + 1;
        TableList tableList = new TableList(4, "ID", "Name", "Unit Price", "Quantity").withUnicode(true);
        tableList.addRow(id + "", pName.trim(), df.format(price) + "", qty + "");
        tableList.print();
        while (true) {
            System.out.format("Are you sure want to add this Product? [Y/y] or [N/n] : ");
            String opt = scanner.nextLine();
            switch (opt.toLowerCase()) {
                case "y":
                    LocalDateTime localDateTime = LocalDateTime.now();
                    String strLocalTimeDate = localDateTime.format(dateFormat);
                    Product p = new Product(id, pName, price, qty, strLocalTimeDate);
//                    prod.add(p);
                    System.out.println();
                    TableList successTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                    successTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()), p.getQty() + "", p.getImportedDate(), "Successfully Added");
                    successTable.print();
                    System.out.println("");
                    query.insert(connection, p, "tblproductstemp");
                    return;
                case "n":
                    System.out.println();
                    return;
            }
        }
    }

    public void insertProduct(ArrayList<Product> prod) {
        System.out.format("\n********** Insert Product **********\n");
        try {
            connection = connectionDB.getConnectionDB();
            int id = query.selectMaxID(connection, "tblproductstemp") + 1;
            int qty = 0;
            double unitPrice = 0;
            System.out.format("=> Product's Id    : %d%n", id);
            if (id < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
            System.out.print("=> Product's Name  : ");
            String pName = scanner.nextLine();
            boolean checkUnitPrice = true;
            while (checkUnitPrice) {
                try {
                    System.out.print("=> Unit Price      : ");
                    unitPrice = scanner.nextDouble();
                    if (unitPrice < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (unitPrice == 0) throw new WrongNumberInputException("Unit Price must be greater than 0!");
                    else checkUnitPrice = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : No Letters Please!");
                }
            }
            boolean checkQty = true;
            while (checkQty) {
                try {
                    System.out.print("=> Quantity        : ");
                    qty = scanner.nextInt();
                    if (qty < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (qty == 0) throw new WrongNumberInputException("Quantity must be greater than 0!");
                    else checkQty = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : Quantity must be Integer & No Letters Please!");
                }
            }
            TableList tableList = new TableList(4, "ID", "Name", "Unit Price", "Quantity").withUnicode(true);
            tableList.addRow(id + "", pName.trim(), df.format(unitPrice) + "", qty + "");
            tableList.print();

            scanner.nextLine();
            while (true) {
                System.out.format("Are you sure want to add this Product? [Y/y] or [N/n] : ");
                String opt = scanner.nextLine();
                switch (opt.toLowerCase()) {
                    case "y":
                        LocalDateTime importedDate = LocalDateTime.now();
                        String strImportedDate = importedDate.format(dateFormat);
                        Product p = new Product(id, pName.trim(), unitPrice, qty, strImportedDate.trim());
//                        prod.add(p);
                        System.out.println();
                        TableList successTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                        successTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()), p.getQty() + "", p.getImportedDate(), "Successfully Added");
                        successTable.print();
                        System.out.println("");
                        query.insert(connection, p, "tblproductstemp");
                        return;
                    case "n":
                        System.out.println();
                        return;
                }
            }
        } catch (InputMismatchException e) {
            scanner.nextLine();
            warningTable("WARNING : No Letters Please!");
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void viewProductByShortcut(ArrayList<Product> prod, int id) {
        int totalProducts = prod.size();
        int count = 0;
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            for (Product p : prod) {
                if (p.getId() == id) {
                    count++;
                    TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
                    tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate());
                    tableList.print();
                }
            }
            if (count == 0) warningTable("Sorry ID \"" + id + "\" Not Found...");
        }
    }

    public void viewProducts(ArrayList<Product> prod) {
        System.out.println("\n********** View Product **********");
        int totalProducts = prod.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            int id;
            int count = 0;
            boolean checkTrue = true;
            while (checkTrue) {
                try {
                    System.out.print("=> Input Product's ID : ");
                    id = scanner.nextInt();
                    if (id < 0) throw new IllegalArgumentException("Only Positive Numbers Please!");
                    for (Product p : prod) {
                        if (id == p.getId()) {
                            count++;
                            System.out.println();
                            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
                            tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate());
                            tableList.print();
                        }
                    }
                    if (count == 0) warningTable("Sorry ID \"" + id + "\" Not Found...");
                    checkTrue = false;
                } catch (IllegalArgumentException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING :Input must be Number & No Letters Please!");
                }
            }
        }
    }

    public void deleteProductByShortcut(ArrayList<Product> prod, int id) throws IOException {
        int totalProducts = prod.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            for (Product p : prod) {
                if (p.getId() == id) {
                    TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
                    tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate());
                    tableList.print();

                    while (true) {
                        System.out.format("Are you sure want to delete this Product? [Y/y] or [N/n] : ");
                        String opt = scanner.nextLine();
                        switch (opt.toLowerCase()) {
                            case "y":
                                LocalDateTime removeDate = LocalDateTime.now();
                                String strRemoveDate = removeDate.format(dateFormat);
                                System.out.println();
                                TableList successRemoveTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Removed Date", "Message").withUnicode(true);
                                successRemoveTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", strRemoveDate, "Successfully Removed");
                                successRemoveTable.print();
                                prod.remove(p);
                                int i = 0;
                                for (Product pr : prod) {
                                    i++;
                                    pr.setId(i);
                                }
                                System.out.println("");
                                return;
                            case "n":
                                System.out.println();
                                return;
                        }
                    }
                }
            }
            warningTable("Sorry ID \"" + id + "\" Not Found...");
        }
    }

    public void deleteProduct(ArrayList<Product> prod) throws IOException {
        System.out.println("\n********** Delete Product **********");
        int totalProducts = prod.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            int id = 0;
            int count = 0;
            boolean checkTrue = true;
            while (checkTrue) {
                try {
                    System.out.print("=> Input Product's ID : ");
                    id = scanner.nextInt();
                    if (id < 0) throw new IllegalArgumentException("Only Positive Numbers Please!");
                    for (Product p : prod) {
                        if (id == p.getId()) {
                            count++;
                            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
                            tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate());
                            tableList.print();

                            scanner.nextLine();
                            while (true) {
                                System.out.format("Are you sure want to delete this Product? [Y/y] or [N/n] : ");
                                String opt = scanner.nextLine();
                                switch (opt.toLowerCase()) {
                                    case "y":
                                        LocalDateTime removeDate = LocalDateTime.now();
                                        String strRemoveDate = removeDate.format(dateFormat);
                                        System.out.println();
                                        TableList successRemoveTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Removed Date", "Message").withUnicode(true);
                                        successRemoveTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", strRemoveDate, "Successfully Removed");
                                        successRemoveTable.print();
                                        prod.remove(p);
                                        int i = 0;
                                        for (Product pr : prod) {
                                            i++;
                                            pr.setId(i);
                                        }
                                        System.out.println("");
                                        return;
                                    case "n":
                                        System.out.println();
                                        return;
                                }
                            }
                        }
                    }
                    if (count == 0) warningTable("Sorry ID \"" + id + "\" Not Found...");
                    checkTrue = false;
                } catch (IllegalArgumentException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING :Input must be Number & No Letters Please!");
                }
            }
        }
    }

    public void updateMenu() {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table menu = new Table(1, BorderStyle.UNICODE_BOX, ShownBorders.SURROUND, false, "");
        menu.addCell(" 1. Update All | 2. Update Name | 3. Update Quantity | 4. Update Unit Price ", cs);
        menu.addCell(" 5. Back to Menu ", cs);
        System.out.println(menu.render());
    }

    public void updateProduct(ArrayList<Product> products) throws IOException {
        System.out.println("\n********** Update Product **********");
        int totalProducts = products.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            int id = 0;
            int count = 0;
            int opt = 0;
            boolean checkTrue = true;
            while (checkTrue) {
                try {
                    System.out.print("=> Input Product's ID : ");
                    id = scanner.nextInt();
                    if (id < 0) throw new IllegalArgumentException("Only Positive Numbers Please!");
                    for (Product p : products) {
                        if (id == p.getId()) {
                            count++;
                            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
                            tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate());
                            tableList.print();
                            while (true) {
                                try {
                                    System.out.println("\nWhat do you want to update ?");
                                    updateMenu();
                                    System.out.print("=> Input Option [1-5] : ");
                                    opt = scanner.nextInt();
                                    break;
                                } catch (InputMismatchException e) {
                                    warningTable("WARNING : Your Input was Invaluable. Input Again Please!");
                                }
                            }
                            switch (opt) {
                                case 1:
                                    updateAll(p);
                                    break;
                                case 2:
                                    updateByName(p);
                                    break;
                                case 3:
                                    updateByQty(p);
                                    break;
                                case 4:
                                    updateByPrice(p);
                                    break;
                                case 5:
                                    return;
                                default:
                                    warningTable("Your Input was Invaluable. Input Again Please!");
                            }
                        }
                    }
                    if (count == 0) warningTable("Sorry ID \"" + id + "\" Not Found...");
                    checkTrue = false;

                } catch (IllegalArgumentException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING :Input must be Number & No Letters Please!");
                }
            }
        }
    }

    public void updateAll(Product p) {
        System.out.println("\n********** Update All **********");
        try {
            int qty = 0;
            double unitPrice = 0;
            System.out.print("=> New Product's Name : ");
            scanner.nextLine();
            String pName = scanner.nextLine();
            boolean checkUnitPrice = true;
            while (checkUnitPrice) {
                try {
                    System.out.print("=> New Unit Price     : ");
                    unitPrice = scanner.nextDouble();
                    if (unitPrice < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (unitPrice == 0) throw new WrongNumberInputException("Unit Price must be greater than 0!");
                    else checkUnitPrice = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : No Letters Please!");
                }
            }
            boolean checkQty = true;
            while (checkQty) {
                try {
                    System.out.print("=> New Quantity       : ");
                    qty = scanner.nextInt();
                    if (qty < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (qty == 0) throw new WrongNumberInputException("Quantity must be greater than 0!");
                    else checkQty = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : Quantity must be Integer & No Letters Please!");
                }
            }
            LocalDateTime newImportedDate = LocalDateTime.now();
            String strNewImportedDate = newImportedDate.format(dateFormat);
            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
            tableList.addRow(p.getId() + "", pName.trim(), df.format(unitPrice) + "", qty + "", strNewImportedDate);
            tableList.print();

            scanner.nextLine();
            while (true) {
                System.out.format("Are you sure want to Update this Product? [Y/y] or [N/n] : ");
                String opt = scanner.nextLine();
                switch (opt.toLowerCase()) {
                    case "y":
                        p.setImportedDate(strNewImportedDate);
                        p.setName(pName);
                        p.setUnitPrice(unitPrice);
                        p.setQty(qty);
                        System.out.println();
                        TableList successTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                        successTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate(), "Successfully Updated");
                        successTable.print();
                        System.out.println("");
                        return;
                    case "n":
                        System.out.println();
                        return;
                }
            }
        } catch (InputMismatchException e) {
            scanner.nextLine();
            warningTable("WARNING : No Letters Please!");
        }
    }

    public void updateByName(Product p) {
        System.out.println("\n********** Update By Name **********");
        try {
            System.out.print("=> New Product's Name : ");
            scanner.nextLine();
            String pName = scanner.nextLine();
            LocalDateTime newImportedDate = LocalDateTime.now();
            String strNewImportedDate = newImportedDate.format(dateFormat);

            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
            tableList.addRow(p.getId() + "", pName.trim(), df.format(p.getUnitPrice()) + "", p.getQty() + "", strNewImportedDate);
            tableList.print();

//            scanner.nextLine();
            while (true) {
                System.out.format("Are you sure want to Update this Product? [Y/y] or [N/n] : ");
                String opt = scanner.nextLine();
                switch (opt.toLowerCase()) {
                    case "y":
                        p.setImportedDate(strNewImportedDate);
                        p.setName(pName.trim());
                        System.out.println();
                        TableList successUpdateTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                        successUpdateTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate(), "Successfully Updated");
                        successUpdateTable.print();
                        System.out.println("");
                        return;
                    case "n":
                        System.out.println();
                        return;
                }
            }
        } catch (Exception e) {
            scanner.nextLine();
            warningTable("WARNING : " + e.getMessage());
        }
    }

    public void updateByQty(Product p) {
        System.out.println("\n********** Update By Quantity **********");
        try {
            int qty = 0;
            boolean checkQty = true;
            while (checkQty) {
                try {
                    System.out.print("=> New Quantity       : ");
                    qty = scanner.nextInt();
                    if (qty < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (qty == 0) throw new WrongNumberInputException("Quantity must be greater than 0!");
                    else checkQty = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : Quantity must be Integer & No Letters Please!");
                }
            }
            LocalDateTime newImportedDate = LocalDateTime.now();
            String strNewImportedDate = newImportedDate.format(dateFormat);

            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
            tableList.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", qty + "", strNewImportedDate);
            tableList.print();

            scanner.nextLine();
            while (true) {
                System.out.format("Are you sure want to Update this Product? [Y/y] or [N/n] : ");
                String opt = scanner.nextLine();
                switch (opt.toLowerCase()) {
                    case "y":
                        p.setImportedDate(strNewImportedDate);
                        p.setQty(qty);
                        System.out.println();
                        TableList successTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                        successTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate(), "Successfully Updated");
                        successTable.print();
                        System.out.println("");
                        return;
                    case "n":
                        System.out.println();
                        return;
                }
            }
        } catch (InputMismatchException e) {
            scanner.nextLine();
            warningTable("WARNING : No Letters Please!");
        }
    }

    public void updateByPrice(Product p) {
        System.out.println("\n********** Update By Price **********");
        try {
            double unitPrice = 0;
            boolean checkUnitPrice = true;
            while (checkUnitPrice) {
                try {
                    System.out.print("=> New Unit Price     : ");
                    unitPrice = scanner.nextDouble();
                    if (unitPrice < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (unitPrice == 0) throw new WrongNumberInputException("Unit Price must be greater than 0!");
                    else checkUnitPrice = false;
                } catch (IllegalArgumentException | WrongNumberInputException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : No Letters Please!");
                }
            }
            LocalDateTime newImportedDate = LocalDateTime.now();
            String strNewImportedDate = newImportedDate.format(dateFormat);
            TableList tableList = new TableList(5, "ID", "Name", "Unit Price", "Quantity", "Imported Date").withUnicode(true);
            tableList.addRow(p.getId() + "", p.getName(), df.format(unitPrice) + "", p.getQty() + "", strNewImportedDate);
            tableList.print();

            scanner.nextLine();
            while (true) {
                System.out.format("Are you sure want to Update this Product? [Y/y] or [N/n] : ");
                String opt = scanner.nextLine();
                switch (opt.toLowerCase()) {
                    case "y":
                        p.setImportedDate(strNewImportedDate);
                        p.setUnitPrice(unitPrice);
                        System.out.println();
                        TableList successTable = new TableList(6, "ID", "Name", "Unit Price", "Quantity", "Imported Date", "Message").withUnicode(true);
                        successTable.addRow(p.getId() + "", p.getName(), df.format(p.getUnitPrice()) + "", p.getQty() + "", p.getImportedDate(), "Successfully Updated");
                        successTable.print();
                        System.out.println("");
                        return;
                    case "n":
                        System.out.println();
                        return;
                }
            }
        } catch (InputMismatchException e) {
            scanner.nextLine();
            warningTable("WARNING : No Letters Please!");
        }
    }

    public void paginationFirst(ArrayList<Product> prod) {
        currentPage = 1;
        displayProductsInRow(displayRow, currentPage, prod);
    }

    public void paginationNext(ArrayList<Product> prod) {
        int totalProducts = prod.size();
        System.out.println(totalProducts);
        int lastPage = totalProducts / displayRow;
        if (totalProducts % displayRow != 0) lastPage += 1;
        if (currentPage >= lastPage) currentPage = lastPage;
        else currentPage += 1;
        displayProductsInRow(displayRow, currentPage, prod);
    }

    public void paginationPrevious(ArrayList<Product> prod) {
        if (currentPage == 1) {
        } else {
            currentPage -= 1;
        }
        displayProductsInRow(displayRow, currentPage, prod);
    }

    public void paginationLast(ArrayList<Product> prod) {
        int totalProducts = prod.size();
        int totalPages = totalProducts / displayRow;
        if (totalProducts % displayRow != 0) totalPages += 1;
        currentPage = totalPages;
        displayProductsInRow(displayRow, currentPage, prod);
    }

    public void searchByProductNameMenu() {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table menu = new Table(1, BorderStyle.UNICODE_BOX, ShownBorders.SURROUND, false, "");
        menu.addCell(" F: First | N: Next | P: Previous | L: Last ", cs);
        menu.addCell(" G: Goto  | B: Back ", cs);
        System.out.println(menu.render());
    }

    public void searchByProductName(ArrayList<Product> prod) {
        System.out.println("\n********** Search Product **********");
        int totalProducts = prod.size();
        if (totalProducts == 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            ArrayList<Product> searchProducts = new ArrayList<>();
            System.out.format("%s", "=> Input Product's Name : ");
            String pName = scanner.nextLine();
            int count = 0;
            for (Product p : prod) {
                if (p.getName().toLowerCase().matches(".*(" + pName.trim().toLowerCase() + ").*")) {
                    count++;
                    searchProducts.add(p);
                }
            }
            if (count > 0) {
                displayProductsInRow(displayRow, 1, searchProducts);
                while (true) {
                    ArrayList<Integer> pageHis = new ArrayList<>();
                    pageHis.add(currentPage);
                    searchByProductNameMenu();
                    System.out.print("=> Input Option : ");
                    String opt = scanner.nextLine().trim().toLowerCase();
                    switch (opt) {
                        case "f":
                            paginationFirst(searchProducts);
                            break;
                        case "n":
                            paginationNext(searchProducts);
                            break;
                        case "p":
                            paginationPrevious(searchProducts);
                            break;
                        case "l":
                            paginationLast(searchProducts);
                            break;
                        case "g":
                            gotoPage(searchProducts);
                            break;
                        case "b":
                            currentPage = pageHis.get(0);
                            return;
                        default:
                            warningTable("Your Input was Invaluable. Input Again Please!");
                    }
                }
            } else {
                warningTable("Sorry match not Found. No Product Name \"" + pName + "\".");
            }
        }
    }

    public void gotoPage(ArrayList<Product> prod) {
        int totalProducts = prod.size();
        if (totalProducts <= 0) {
            warningTable("Sorry No Product In Stock. Please Input It First !");
            System.out.println();
        } else {
            boolean checkTrue = true;
            while (checkTrue) {
                try {
                    int lastPage = totalProducts / displayRow;
                    if (totalProducts % displayRow != 0) lastPage += 1;
                    System.out.print("=> Goto Page : ");
                    currentPage = scanner.nextInt();
                    if (currentPage < 0)
                        throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                    else if (currentPage == 0) currentPage = 1;
                    else if (currentPage > lastPage) currentPage = lastPage;
                    displayProductsInRow(displayRow, currentPage, prod);
                    checkTrue = false;
                } catch (IllegalArgumentException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING : No Letters Please!");
                }
            }
        }
    }

    public void setDisplayRow() {
        System.out.println("\n********** Set Display Row **********");
        int totalProducts = products.size();
        boolean checkTrue = true;
        while (checkTrue) {
            try {
                System.out.print("=> Set Display Row : ");
                displayRow = scanner.nextInt();
                if (displayRow < 0) throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
                else if (displayRow == 0)
                    throw new WrongNumberInputException("Row Number must be Greater than 0 Please!");
                else if (displayRow > totalProducts) {
                    displayRow = totalProducts;
                    checkTrue = false;
                } else checkTrue = false;
            } catch (IllegalArgumentException | WrongNumberInputException e) {
                warningTable("WARNING : " + e.getMessage());
            } catch (InputMismatchException e) {
                scanner.nextLine();
                warningTable("WARNING : No Letters Please!");
            }
        }
        scanner.nextLine();
    }

    public void add10MRecord(ArrayList<Product> prod, double num) throws IOException {
        System.out.println("\n********** Add Millions Records **********");
        int MILLION = 1000000;
        num = num * MILLION;
        int id = prod.size() + 1;
        for (int i = 0; i < num; i++) {
            LocalDateTime importedDate = LocalDateTime.now();
            String strImportedDate = importedDate.format(dateFormat);
            Product product = new Product(id + i, "P-00" + i, 0, 0, strImportedDate);
            prod.add(product);
        }
    }

    public void save() throws IOException {
        saveSetting();
        saveData(products);
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.right, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table footer = new Table(2, BorderStyle.DESIGN_CURTAIN_WIDE, ShownBorders.SURROUND, false, "");
        footer.addCell("\tSaved Successfully\t", cs);
        System.out.println(footer.render());
    }

    public void saveData(ArrayList<Product> prod) throws IOException {
        
    }

    public void saveSetting() {

    }

    public void backupData() throws IOException {
        DateTimeFormatter dateFormatFileName = DateTimeFormatter.ofPattern("MM-dd-yy-HH-mm-ss");
        LocalDateTime importedDate = LocalDateTime.now();
        String strImportedDate = importedDate.format(dateFormatFileName);

        String path = "src\\backup\\" + strImportedDate + ".bak";
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        for (Product p : products) {
            objectOutputStream.writeObject(p);
        }
        outputStream.flush();
        outputStream.close();
        System.out.println("\no~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o");
        System.out.println(" Backup Successfully to New Backup File: " + strImportedDate + ".bak");
        System.out.println("o~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o\n");
    }

    public void restore() {
        Scanner scanner = new Scanner(System.in);
        File folder = new File("src\\backup");
        File[] listOfFiles = folder.listFiles();

        int fileNum = 0;
        boolean checkTrue = true;

        if (listOfFiles.length == 0) {
            System.out.println("\no~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o");
            System.out.println("    There is no backup file to restore");
            System.out.println("o~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o\n");
        } else {
            System.out.println("\n********** Please choose file Backup **********");
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    System.out.println((i + 1) + ") " + listOfFiles[i].getName());
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                }
            }

            while (checkTrue) {
                try {
                    System.out.println("Note: Enter 0 (zero) to go back!");
                    System.out.print("Choose File Now: ");
                    fileNum = scanner.nextInt();
                    if (fileNum > 0 && fileNum <= listOfFiles.length) {
                        checkTrue = false;
                    }
                    if (fileNum == 0) return;
                    if (fileNum > listOfFiles.length)
                        throw new IllegalArgumentException("Please choose the correct file number!");
                    if (fileNum < 0) throw new IllegalArgumentException("Only Positive Numbers Please!");
                    fileNum = fileNum - 1;
                } catch (IllegalArgumentException e) {
                    warningTable("WARNING : " + e.getMessage());
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    warningTable("WARNING :Input must be Number & No Letters Please!");
                }
            }
            String fileName = listOfFiles[fileNum].getName();
            try {
                File file = new File("src\\backup\\" + fileName);
                InputStream inputStream = new FileInputStream(file);
                if (file.length() != 0) {
                    products.clear();
                    ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                    boolean check = true;
                    while (check) {
                        if (inputStream.available() != 0) {
                            Product p = (Product) objectInputStream.readObject();
                            products.add(p);
                        } else check = false;
                    }
                    saveData(products);
                    objectInputStream.close();
                    System.out.println("\no~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o");
                    System.out.println("  Restore Successfully from Backup File: " + listOfFiles[fileNum].getName());
                    System.out.println("o~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o\n");
                }
            } catch (ClassNotFoundException | FileNotFoundException e) {
                e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void help() {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop, CellStyle.NullStyle.emptyString);
        Table menu = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND, false, "");
        menu.addCell(" 1.   Press   * : Display all record of products ", cs);
        menu.addCell(" 2.   Press   w : Add new product ", cs);
        menu.addCell("      Press   w#proname-unitprice-qty : Shortcut for add new product ", cs);
        menu.addCell(" 3.   Press   r : Read Content any content ", cs);
        menu.addCell("      Press   r#proId : Shortcut for read product by Id ", cs);
        menu.addCell(" 4.   Press   u : Update data ", cs);
        menu.addCell(" 5.   Press   d : Delete data ", cs);
        menu.addCell("      Press   d#proId : Shortcut for delete product by Id ", cs);
        menu.addCell(" 6.   Press   f : Display first page ", cs);
        menu.addCell(" 7.   Press   p : Display previous page ", cs);
        menu.addCell(" 8.   Press   n : Display next page ", cs);
        menu.addCell(" 9.   Press   l : Display last page ", cs);
        menu.addCell(" 10.  Press   s : Search product by name ", cs);
        menu.addCell(" 11.  Press   sa: Save record to file ", cs);
        menu.addCell(" 12.  Press   se: Set display row ", cs);
        menu.addCell(" 13.  Press   ba: Backup data ", cs);
        menu.addCell(" 14.  Press   re: Restore data ", cs);
        menu.addCell(" 15.  Press   h : Help ", cs);
        menu.addCell(" 16.  Press   _numM : Add millions products ", cs);
        menu.addCell(" 17.  Press   e : Exit ", cs);
        System.out.println(menu.render());
    }
}