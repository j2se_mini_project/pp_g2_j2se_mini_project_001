package com.company;

import java.sql.*;

public class Query {

    public void insert(Connection connection, Product product, String table) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + table + " VALUES (?, ?, ?, ?, ?)");
            ps.setInt(1, product.getId());
            ps.setString(2, product.getName());
            ps.setDouble(3, product.getUnitPrice());
            ps.setInt(4, product.getQty());
            ps.setDate(5, Date.valueOf(product.getImportedDate()));
            int insert = ps.executeUpdate();
            System.out.println("*** Result : " + insert + " Row Insert ***");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void delete(Connection connection, Product product) {

    }

    public int selectMaxID(Connection connection, String table) throws SQLException {
        int id = 0;
        try {
            String query = "SELECT MAX(product_id) FROM " + table;
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ResultSet r = resultSet;
                System.out.println((r.getInt(1)));
                id = r.getInt(1);
            }
        } catch (NullPointerException e) {
            id = 0;
        }
        return id;
    }
}
