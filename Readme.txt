					   Welcome to
					Stock Management

  _____    _                                   _____                   _        _____   ___  
 |  __ \  | |                                 |  __ \                 | |      / ____| |__ \ 
 | |__) | | |__    _ __     ___    _ __ ___   | |__) |   ___   _ __   | |__   | |  __     ) |
 |  ___/  | '_ \  | '_ \   / _ \  | '_ ` _ \  |  ___/   / _ \ | '_ \  | '_ \  | | |_ |   / / 
 | |      | | | | | | | | | (_) | | | | | | | | |      |  __/ | | | | | | | | | |__| |  / /_ 
 |_|      |_| |_| |_| |_|  \___/  |_| |_| |_| |_|       \___| |_| |_| |_| |_|  \_____| |____|


## Introduction

Stock Mangagement System allow you to add product, store and manage many data of production.
It outputs the data into table which is very easy to look and recongize.

=================================================================================================

## Installation

Step 1: Go to folder directory that you want to store the project.
Step 2: Right click and select Git Bash Here.
Step 3: Paste the git clone statement below for cloning the project from server:

statement: git clone https://gitlab.com/j2se_mini_project/pp_g2_j2se_mini_project_001.git

=================================================================================================

## Usage

1.   Press   * : Display all record of products                     
2.   Press   w : Add new product                                    
     Press   w#proname-unitprice-qty : Shortcut for add new product 
3.   Press   r : Read Content any content                           
     Press   r#proId : Shortcut for read product by Id              
4.   Press   u : Update data                                        
5.   Press   d : Delete data                                        
     Press   d#proId : Shortcut for delete product by Id            
6.   Press   f : Display first page                                 
7.   Press   p : Display previous page                             
8.   Press   n : Display next page                                  
9.   Press   l : Display last page                                  
10.  Press   s : Search product by name                             
11.  Press   sa: Save record to file                                
12.  Press   se: Set display row                                    
13.  Press   ba: Backup data                                        
14.  Press   re: Restore data                         
15.  Press   h : Help                                               
16.  Press   _numM : Add millions products                          
17.  Press   e : Exit 

=================================================================================================

## Contributing

Feedbacks are welcome! Please kindly give us any feedbacks if you have.

## License
https://gitlab.com/j2se_mini_project/pp_g2_j2se_mini_project_001.git